var myApp = angular.module('myApp',[]);

myApp.controller('ContactController', function($scope){

    $scope.contacts = [
        { id:0, 'name': 'imm', 
          'email':'emmanuel.adeojo@yahoo.com', 
          'phone': '08149848925',
          'message': 'testing'
        },
        { id:1, 'name': 'toluwanimi', 
          'email':'ayoola.toluwanimi@lmu.edu.ng', 
          'phone': '08149848925',
          'message': 'testing'
        }
    ];
    $scope.newcontact = {};
    var uid = 2;

    $scope.saveContact = function() {
        if($scope.newcontact.id == null) {
        //if this is new contact, add it in contacts array
        $scope.newcontact.id = uid++;
        $scope.contacts.push($scope.newcontact);
        } else {
            //for existing contact, find this contact using id
            //and update it.
            for(i in $scope.contacts) {
                if($scope.contacts[i].id == $scope.newcontact.id) {
                $scope.contacts[i] = $scope.newcontact;
                }
            }
        }
    }

    $scope.delete = function(id) {
        for(i in $scope.contacts) {
            if($scope.contacts[i].id == id) {
                $scope.contacts.splice(i,1);
                $scope.newcontact = {};
            }
        }
    }

    $scope.edit = function(id) {
    //search contact with given id and update it
        for(i in $scope.contacts) {
            if($scope.contacts[i].id == id) {
                //we use angular.copy() method to create 
                //copy of original object
                $scope.newcontact = angular.copy($scope.contacts[i]);
                $('#empForm').slideUp();
                $('#editForm').slideToggle();
            }
        }
    }

    $scope.show_form = true;
    $scope.formToggle =function(){
        $('#empForm').slideToggle();
        $('#editForm').css('display', 'none');
    }
})

